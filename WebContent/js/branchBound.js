var nodes_BB = new Array();
var expandir;
var nodeRaiz_BB;
var LBB=0, UBB=0, L;
var lb_expandido, ub_expandido;

function lubound(itens, pesoRestante, valorCorrente, k){
	var c = pesoRestante;
	LBB = valorCorrente;
	var n = itens.length;
	for(var i = k; i < n; i++){
		var item = itens[i];
		if(c < item.peso){
			UBB = LBB + (c * item.chave);
			for (var j = i+1;j<n;j++){
				var item_sub = itens[j];
				if(c >= item_sub.peso){
					c -= item_sub.peso;
					LBB += item_sub.valor;
				}
			}
			formataLimites();
			return;
		}
		c -= item.peso;
		LBB += item.valor;
	}
	UBB = LBB;
	formataLimites();
}

function formataLimites(){
	UBB = parseFloat(UBB.toPrecision(4));
	LBB = parseFloat(LBB.toPrecision(4));
}

function inicializaListaExpancao(){
	nodeRaiz_BB = new node_bb("raiz",new Array());
	expandir = new Array();
	expandir.push(nodeRaiz_BB);
}

function getLargest(){
	expandir.sort(compara_no);
	return expandir.pop();
}

function adicionaNovoNo(item, parent, level, tag, capacidadeDisponivel, pe, ub, lbb){
	var newNode_bb = new node_bb("item " + item.id, new Array(), parent, level, capacidadeDisponivel, pe, ub, lbb, tag, item);
	if(parent.children == undefined){
		parent.children = new Array();
	}
	parent.children.splice(0,0, newNode_bb);
	expandir.push(newNode_bb);
}

function atualizaResultItens(L, nodeAns){
	var temp = nodeAns;
	pesoInserido = 0;
	valorInserido = 0;
	do{
		if(temp.item != null){
			temp.item.result = temp.tag;
			pesoInserido += temp.item.peso * temp.tag;
			valorInserido += temp.item.valor * temp.tag;
		}
		temp = temp.parent;
	}while(temp);
}

function voltaItens(itens){
	itens.forEach(function(element,index,a){
		itens[index].result = 0;
	});
}

function executaBranchBound(itens, M, s){
	var itensCopia = itens.slice();
	itensCopia.splice(0,0,new item(0,0,0));
	inicializaListaExpancao();
	var n = itensCopia.length;
	var i, prof, cap;
	var nodeAns;
	nodeAtual = expandir.pop();
	nodeAtual.level = 1;
	nodeAtual.capacidadeDisponivel = M;
	nodeAtual.pe = 0;
	lubound(itensCopia, M, 0, 1);
	L = LBB - s;
	nodeAtual.ub = UBB;
	var acabou = false;
	$('html').keypress(function(e){
		if(e.keyCode == 46 && !acabou){
			i = nodeAtual.level;
			cap = nodeAtual.capacidadeDisponivel;
			prof = nodeAtual.pe;
			if(i == n){
				if(prof > L){
					L = prof;
					nodeAns = nodeAtual;
				}
			}else {
				if(cap >= itensCopia[i].peso){
					adicionaNovoNo(itensCopia[i]
									,nodeAtual
									,nodeAtual.level+1
									,1
									,cap - itensCopia[i].peso
									,prof + itensCopia[i].valor
									,nodeAtual.ub
									,LBB);
				}
				lubound(itensCopia, cap, prof, i+1);
				if(UBB > L){
					adicionaNovoNo(itensCopia[i]
					,nodeAtual
					,nodeAtual.level+1
					,0
					,cap
					,prof
					,UBB
					,LBB);
					
					L = Math.max(L, LBB-s);
				}
			}
			if(expandir.length > 0){
				nodeAtual = getLargest();
			}
			if(nodeAtual.ub <= L){
				acabou = true;
			}
			lb_expandido = nodeAns ? nodeAns.lb : nodeAtual.lb;
			ub_expandido = nodeAns ? nodeAns.ub : nodeAtual.ub;
			
			atualizaResultItens(L, nodeAns ? nodeAns : nodeAtual);
			atualizatabelaDetalhada(itens, atualizaArvoreBranch(nodeRaiz_BB));
		}else if(e.keyCode == 44){
			voltaItens(itens);
			$("#viz").empty();
			$("#viz").removeClass("efeito-adicional");
			itensCopia = itens.slice();
			itensCopia.splice(0,0,new item(0,0,0));
			nodeAns = undefined;
			UBB = 0;
			LBB = 0;
			L = 0;
			n = itensCopia.length;
			inicializaListaExpancao();
			nodeAtual = expandir.pop();
			nodeAtual.level = 1;
			nodeAtual.capacidadeDisponivel = M;
			nodeAtual.pe = 0;
			pesoInserido = 0;
			valorInserido = 0;
			lubound(itensCopia, M, 0, 1);
			L = LBB - s;
			nodeAtual.ub = UBB;
			acabou = false;
			lb_expandido = "";
			ub_expandido = "";
			atualizatabelaDetalhada(itens, atualizaLinhaBranchBound());			
		}
			
	});
}