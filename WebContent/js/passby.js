var itens = new Array();
var tableDetalhada;

function montaItens(){
	var vector = table.$('.valores');
	var it;
	var countInput = 1;
	var valor;
	itens = new Array();
	vector.each(function(d,e){
		if(countInput % 2 == 0){
			it = new item(itens.length+1,parseInt(valor),parseInt($(e).val()));
			itens.push(it);
		}else {
			valor = $(e).val();
		}
		countInput++;
	});
	
	itens;
}

function passByGuloso(){
	montaItens();
	itens.sort(comparar);
	executaGuloso(itens, parseInt($("#pesoMochila").val()));
}

function passByBacktracking(){
	montaItens();
	itens.sort(comparar);
	executaBacktracking(itens, parseInt($("#pesoMochila").val()));
}

function passByBranchBound(){
	montaItens();
	itens.sort(comparar);
	executaBranchBound(itens, parseInt($("#pesoMochila").val()), 1);
}

function passByDinamico(){
	montaItens();
	executaDinamico(itens, parseInt($("#pesoMochila").val()));
}

function trataPercorrer(metodo){
	pesoTotal = $("#pesoMochila").val();
	pesoInserido = 0;
	valorInserido = 0;
	modificaMochila(0);
	tableDetalhada = tabelaDetalhadaParaItens(metodo);
};

function achaSeta(element, index, array){
	if(element != ""){
		return element;
	}
}

function setaSolucaoOtimaBacktracking(){
	var vetorValores = tableDetalhada.row(0).data();
	var vetorPeso = tableDetalhada.row(1).data();

	var tamanhoVetorPeso = vetorPeso.length;
	var tamanhoVetorValores = vetorValores.length;
	
	vetorPeso[tamanhoVetorPeso - 1] = solOtima.peso;
	vetorValores[tamanhoVetorValores - 1] = solOtima.valor;
	
	tableDetalhada.row(3).data(solOtima.itensSol).draw().node();
	tableDetalhada.row(0).data(vetorValores).draw().node();
	tableDetalhada.row(1).data(vetorPeso).draw().node();
	
	modificaMochila((solOtima.peso * 100) / pesoTotal);
}

function trataSolucaoOtimaBacktracking(vetor){
	if(valorInserido > solOtima.valor){
		solOtima.valor = valorInserido;
		solOtima.peso = pesoInserido;
		solOtima.itensSol = vetor.slice();
	}
}

function atualizatabelaDetalhada(itens,callback){
	var vetorSituacao = tableDetalhada.row(3).data();
	var vetorPeso = tableDetalhada.row(1).data();
	var vetorValores = tableDetalhada.row(0).data();
	
	var tamanhoVetorPeso = vetorPeso.length;
	var tamanhoVetorValores = vetorValores.length;
	
	itens.forEach(function(element,index,a){
		vetorSituacao[index+1] = itens[index].result;
	});
	
	if(callback)
		callback();
	
	vetorPeso[tamanhoVetorPeso - 1] = pesoInserido;
	vetorValores[tamanhoVetorValores - 1] = valorInserido;
	
	tableDetalhada.row(0).data(vetorValores).draw().node();
	tableDetalhada.row(1).data(vetorPeso).draw().node();
	tableDetalhada.row(3).data(vetorSituacao).draw().node();
	
	modificaMochila((pesoInserido * 100) / pesoTotal);
	
	return vetorSituacao;
}

function atualizaSeta(){
	var vetorSeta = tableDetalhada.row(4).data();
	
	var seta = vetorSeta.find(achaSeta);
	var indexSeta = vetorSeta.indexOf(seta);
	
	if(indexSeta+1 < vetorSeta.length - 1){
		vetorSeta[indexSeta+1] = vetorSeta[indexSeta];
		vetorSeta[indexSeta] = "";		
	}
	
	tableDetalhada.row(4).data(vetorSeta).draw().node();
}

function iniciaArvore(raiz){
	$("#viz").addClass("efeito-adicional");
	criaTree(raiz,'B');	
}

function atualizaArvore(raiz){
	$("#viz").empty();
	criaTree(raiz,'B');
	var vetorFuncao = tableDetalhada.row(4).data();
	vetorFuncao[vetorFuncao.length-1] = limite;
	tableDetalhada.row(4).data(vetorFuncao).draw().node();
}

function atualizaArvoreBranch(raiz){
	$("#viz").empty();
	$("#viz").addClass("efeito-adicional");
	criaTree(raiz,'BB');	
	atualizaLinhaBranchBound();
}

function atualizaLinhaBranchBound(){
	var vetorFuncoesResultBranchBound = tableDetalhada.row(4).data();
	vetorFuncoesResultBranchBound[vetorFuncoesResultBranchBound.length - 1] = "LB: " + lb_expandido + " UB: " + ub_expandido;
	tableDetalhada.row(4).data(vetorFuncoesResultBranchBound).draw().node();
}


function tabelaDetalhadaParaItens(metodo){
	var linhaPesos = new Array("Peso");
	var linhaValores = new Array("Valor");
	var linhaValorPorPeso = new Array("Valor/Peso");
	var linhaSelecionado = new Array("Seleção");
	var linhaSeta = new Array("");
	var linhaFuncaoLimite = new Array("Bound");
	var linhaFuncoesResultBranchBound = new Array("Avaliação");
	var qtd = itens.length;
	$("#divResposta").empty();
	$("#divResposta").append(meRetorneUmaTable("resposta"));
	var lHead = $("#resposta thead tr");
	lHead.append("<th>Info</th>");
	var imagem = '../images/SetaCima.png';
	for (var int = 0; int < qtd; int++) {
		var item = itens[int];
		linhaValores.push(item.valor);
		linhaPesos.push(item.peso);
		linhaValorPorPeso.push(item.chave.toPrecision(4));
		linhaSelecionado.push(0);
		linhaSeta.push(int == 0 ? "<img src = "+imagem+" />" : "");
		linhaFuncaoLimite.push("");
		linhaFuncoesResultBranchBound.push("");
		lHead.append("<th>Item"+ item.id +"</th>");		
	}
	
	linhaPesos.push(0);
	linhaValores.push(0);
	linhaValorPorPeso.push("");
	linhaSelecionado.push("");
	linhaSeta.push("");
	linhaFuncaoLimite.push("&infin;");
	linhaFuncoesResultBranchBound.push("LB: " + " UB: ");
	lHead.append("<th>Máximo</th>");
	
	var table = criarDataTable($("#resposta"), 1100);
	table.row.add(linhaValores).draw().node();
	table.row.add(linhaPesos).draw().node();
	table.row.add(linhaValorPorPeso).draw().node();
	table.row.add(linhaSelecionado).draw().node();
	if(metodo=="G" || metodo =="D")
		table.row.add(linhaSeta).draw().node();
	else if(metodo=="B")
		table.row.add(linhaFuncaoLimite).draw().node();
	else if(metodo=="BB")
		table.row.add(linhaFuncoesResultBranchBound).draw().node();
	return table;
}