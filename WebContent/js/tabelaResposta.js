function preencheTabela(table,dados, metodo){
	
	if(metodo == 'T'){
		var conjuntoGuloso = new Array();
		var conjuntoBacktracking = new Array();
		var conjuntoProgDinamica = new Array();
		var conjuntoBranchBound = new Array();
		conjuntoGuloso.push('Guloso');
		conjuntoBacktracking.push('Backtracking');
		conjuntoProgDinamica.push('Dinamico');
		conjuntoBranchBound.push('Branch&Bound');
		
		var pesoTotalGuloso = 0;
		var valorTotalGuloso = 0;
		var pesoTotalBacktracking = 0;
		var valorTotalBacktracking = 0;
		var pesoTotalProgDinamica = 0;
		var valorTotalProgDinamica = 0;
		var pesoTotalBranchBound = 0;
		var valorTotalBranchBound = 0;
		
		dados.forEach(function(d){
			
			conjuntoBacktracking.push(d.resultB);
			pesoTotalBacktracking+=d.peso * d.resultB;
			valorTotalBacktracking+=d.importancia * d.resultB;
			
			conjuntoGuloso.push(d.resultG);
			pesoTotalGuloso+=d.peso * d.resultG;
			valorTotalGuloso+=d.importancia * d.resultG;
			
			conjuntoProgDinamica.push(d.resultD);
			pesoTotalProgDinamica+=d.peso * d.resultD;
			valorTotalProgDinamica+=d.importancia * d.resultD;
			
			conjuntoBranchBound.push(d.resultBB);
			pesoTotalBranchBound+=d.peso * d.resultBB;
			valorTotalBranchBound+=d.importancia * d.resultBB;
			
		});
		conjuntoGuloso.push(pesoTotalGuloso);
		conjuntoGuloso.push(valorTotalGuloso);
		
		conjuntoBacktracking.push(pesoTotalBacktracking);
		conjuntoBacktracking.push(valorTotalBacktracking);
		
		conjuntoProgDinamica.push(pesoTotalProgDinamica);
		conjuntoProgDinamica.push(valorTotalProgDinamica);
		
		conjuntoBranchBound.push(pesoTotalBranchBound);
		conjuntoBranchBound.push(valorTotalBranchBound);
		
		table.row.add(conjuntoGuloso).draw().node();
		table.row.add(conjuntoBacktracking).draw().node();
		table.row.add(conjuntoProgDinamica).draw().node();
		table.row.add(conjuntoBranchBound).draw().node();
		
		
	}else {
		var conjunto = new Array();
		var pesoTotal = 0;
		var valorTotal = 0;
		dados.forEach(function(d){
			if(metodo == 'B'){
				conjunto.push(d.resultB);
				pesoTotal+=d.peso * d.resultB;
				valorTotal+=d.importancia * d.resultB;
			}else if(metodo == 'G'){
				conjunto.push(d.resultG);
				pesoTotal+=d.peso * d.resultG;
				valorTotal+=d.importancia * d.resultG;
			}else if(metodo == 'D'){
				conjunto.push(d.resultD);
				pesoTotal+=d.peso * d.resultD;
				valorTotal+=d.importancia * d.resultD;
			}else if(metodo == 'BB'){
				conjunto.push(d.resultBB);
				pesoTotal+=d.peso * d.resultBB;
				valorTotal+=d.importancia * d.resultBB;
			}
		});		
		conjunto.push(pesoTotal);
		conjunto.push(valorTotal);
		table.row.add(conjunto).draw().node();
		
		return pesoTotal;
	}
};

function setaItemEscolhidoNaTentativa(itemId,result){
	var vetor = tableTentativa.row().data();
	itemId = parseInt(itemId) - 1;
	var tamanho = vetor.length;
	vetor.forEach(function(element,index,a){
		if(index == itemId){
			vetor[index] = result;
		}
	});
	
	vetor[tamanho - 1] = valorInserido;
	vetor[tamanho - 2] = pesoInserido;
	
	tableTentativa.row().data(vetor).draw().node();
};

function tabelaParaTentativa(){
	var conjunto = new Array();
	qtd = parseInt($("#qtdItens").val());
	$("#divResposta").empty();
	$("#divResposta").append(meRetorneUmaTable("resposta"));
	var lHead = $("#resposta thead tr");
	for (var int = 1; int < qtd+1; int++) {
		conjunto.push(0);
		lHead.append("<th>Item"+ int +"</th>");		
	}
	conjunto.push(0);
	conjunto.push(0);
	lHead.append("<th>Peso total</th>");
	lHead.append("<th>Valor total</th>");
	var table = criarDataTable($("#resposta"), 1100);
	table.row.add(conjunto).draw().node();
	return table;
}

function meRetorneUmaTable(id){
	return "<table id='"+id+"' class='display' cellspacing='0' width='100%'> " 
				+"<thead>"
					+"<tr></tr>"
				+"</thead>"
				+"<tbody>"
				+"</tbody>"
				+"</table>";
};

function criarDataTable(minhaTable, sizeScrollX){
	var table = minhaTable.DataTable({
		"scrollX": sizeScrollX,
		"scrollCollapse": true,
		"paging": false,
		"autoWidth" : true,
		"ordering" : false,
		"searching" : false,
		"info": false,
		"columnDefs": [{
		      "targets": 'sorting_disabled',
		      "className" : "dt-body-center"
		    }]
	});
	
	
	return table;
}

function constroiATabelaResposta(dados, metodo){
	$("#divResposta").empty();
	$("#divResposta").append(meRetorneUmaTable("resposta"));
	var lHead = $("#resposta thead tr");
	var qtd = dados.length;
	if(metodo == 'T'){
		lHead.append("<th>Metodo</th>");
	}
	for (var int = 1; int < qtd+1; int++) {
		lHead.append("<th>Item"+ int +"</th>");		
	}
	lHead.append("<th>Peso total</th>");
	lHead.append("<th>Valor total</th>");
	var table = criarDataTable($("#resposta"), 1100);
	
	pesoInserido = preencheTabela(table, dados, metodo);
	return pesoInserido;
};

function constroiMatrizRespostaDinamica(matriz){
	$("#viz").append(meRetorneUmaTable("matrizDinamica"));
	
	var pesoMochila = parseInt($("#pesoMochila").val()) + 1;
	console.log(pesoMochila);
	var meuHead = $("#matrizDinamica thead tr");
	
	for (var int = 0; int < pesoMochila; int++) {
		console.log(int + " " + pesoMochila + " " + int < pesoMochila);
		meuHead.append("<th>"+ int +"</th>");		
	}
	
	var table = criarDataTable($("#matrizDinamica"), 1400);
	
	matriz.forEach(function(linha){
		table.row.add(linha).draw().node();
	});
};
