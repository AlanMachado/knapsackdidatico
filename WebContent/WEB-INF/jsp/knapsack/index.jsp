<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Teste Knapsack Didatico</title>

<link rel="stylesheet" href="../css/style-body.css" type="text/css" media="screen, projection"/>
<link rel="stylesheet" media="screen" href="../css/superfish.css">
<link rel="stylesheet" media="screen" href="../css/superfish-navbar.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/dt/dt-1.10.9,b-1.0.3/datatables.min.css" />
<script type="text/javascript" charset="utf8" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/r/dt/dt-1.10.9,b-1.0.3/datatables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.5/d3.min.js"></script>
<script src="../js/hoverIntent.js"></script>
<script src="../js/superfish.js"></script>
<script type="text/javascript" src="../js/tabelaResposta.js"></script>
<script type="text/javascript" src="../js/arvore.js"></script>
<script type="text/javascript" src="../js/guloso.js"></script>
<script type="text/javascript" src="../js/node_tree.js"></script>
<script type="text/javascript" src="../js/Item.js"></script>
<script type="text/javascript" src="../js/main.js"></script>
<script type="text/javascript" src="../js/menu_events.js"></script>
<script type="text/javascript" src="../js/passby.js"></script>
<script type="text/javascript" src="../js/backtracking.js"></script>
<script type="text/javascript" src="../js/branchBound.js"></script>
<script type="text/javascript" src="../js/dinamico.js"></script>
</head>
<body>
<div id="hell">
	<div id="topo">
		<img src = "<c:url value='/images/unifor1.jpg' />" />
	</div>
	<div id="page-wrap">
        <ul class="sf-menu" id="example">
        	<li><a id="definirMochila" class="naoTentando naoPercorrendo" href="#">Mochila</a>
        		<ul>
        			<li><a id="exemplo1" class="naoTentando naoPercorrendo" href="#">1º Exemplo</a></li>
        			<li><a id="exemplo2" class="naoTentando naoPercorrendo" href="#">2º Exemplo</a></li>
        			<li><a id="exemplo3" class="naoTentando naoPercorrendo" href="#">3º Exemplo</a></li>
        			<li><a id="exemplo4" class="naoTentando naoPercorrendo" href="#">4º Exemplo</a></li>
        		</ul>
        	</li>
        	<li><a id="tentativa" class="naoPercorrendo" href="#">Tentar</a></li>
        	<li><a href="#">Executar Método</a>
        		<ul >
        			<li><a id="executarGuloso" class="executor naoTentando naoPercorrendo" href="#">Guloso</a></li>
        			<li><a id="executarBacktracking" class="executor naoTentando naoPercorrendo" href="#">Backtracking</a></li>
        			<li><a id="executarDinamico" class="executor naoTentando naoPercorrendo" href="#">Programação Dinâmica</a></li>
        			<li><a id="executarBranch" class="executor naoTentando naoPercorrendo" href="#">Branch and Bound</a></li>
        			<li><a id="executarTodos" class="executor naoTentando naoPercorrendo" href="#">Comparativo</a></li>
        		</ul>
        	</li>
        	<li><a href="#">Passo a passo</a>
        		<ul >
        			 <li><a id="percorreGuloso" class="percorrer naoTentando" href="#">Guloso</a></li>
        			 <li><a id="percorreBacktracking" class="percorrer naoTentando" href="#">Backtracking</a></li>
        			 <li><a id="percorreDinamico" class="percorrer naoTentando" href="#">Programação Dinâmica</a></li>
        			 <li><a id="percorreBranchBound" class="percorrer naoTentando" href="#">Branch and Bound</a></li>
        		</ul>
        	</li>
        </ul>
	</div>
	<div class="testeDiv">
		<div id="elementoMochila" class="div-esq">
			<form action="<c:url value='/knapsack/executar' />" id="meuForm">
				<input type="hidden" id="defineMetodo" name="metodo">
				Capacidade: <input type="text" id="pesoMochila" name="peso" size="4" style="margin-top: 10px"/> <br>
			</form>
			Quantidade de itens: <input type="text" id="qtdItens" value="10" size="3" style="margin: 10px" />
			<table id="itens" class="display" cellspacing="0" width="100%" align="left">
				<thead>
					<tr>
						<th>Id</th>
						<th>Valor</th>
						<th>Peso</th>
						<th>Inserir</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
		<div id="divResposta"></div>
		<div id="showKnapsack" class="div-dir">
			<div id="mochila"></div>
		</div>
		<div id="viz"></div>
		<div id="divGambis" style="clear: left"></div>
	</div>
</div>	
</body>
</html>