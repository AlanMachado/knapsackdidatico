var tentando = false;
var pesoInserido = 0;
var valorInserido = 0;
var pesoTotal = 0;
var tableTentativa;
var table;

function modificaTamanhoTable(d, table) {
	if ($.isNumeric(d)) {
		for (i = 0; i < d; i++) {
			var c = i + 1;
			table.row.add(
					[c + "<input type='hidden' id='id"+i+"' name='itens["+i+"].id' size='2' value='"+c+"'/>",
					"<input type='text' class='valores' id='imp"+i+"' name='itens["+i+"].importancia' size='2' />",
					"<input type='text' class='valores' id='peso"+i+"' name='itens["+i+"].peso' size='2' />" ,
					"<input type='checkbox' id='check"+c+"' name='checkTentar' value="+c+" size='2' disabled/>"]).draw().node();
		}
	} else {
		alert("Digite um numero");
	}
};

function modificaMochila(porcentagem){
	var mochila = $("#mochila");
	mochila.width(porcentagem+"%");
	mochila.height("20px");
	mochila.css({
	    backgroundColor: "green"
	  });
	
}

function representacaoMochila(){
	var container = $("#viz");
	container.addClass("efeito-adicional");
	console.log(container.width())
	console.log(container.height());
	container.height(30);
	container.append("<div id='mochila'></div>");
};

function resetaDivViz(){
	$("#viz").height('auto');
	$("#viz").empty();
	$("#viz").removeClass("efeito-adicional");
}

function habilitaTentativa(){
	pesoInserido = 0;
	valorInserido = 0;
	tentando = true;
	modificaMochila(0);
	pesoTotal = $("#pesoMochila").val();
	$("#divResposta").empty();
	$("#itens input[name='checkTentar']").removeAttr('disabled');
	tableTentativa = tabelaParaTentativa();
}

function saindoTentativa(){
	tentando = false;
	table.page(0).draw(false);
	$("#itens input[name='checkTentar']").prop('checked',false);
	$("#itens input[name='checkTentar']").prop('disabled',true);
	$("#itens input[type='text']").removeAttr('disabled');
}

function executaMetodo(algoritmo){
	pesoTotal = $("#pesoMochila").val();
	var pesoInserido = 0;
	var vector = table.$('.valores');
	var execute = true;
	vector.each(function(d,e){
		var valor = $(e).val();
		if(valor <= 0 || valor == ""){
			execute = false;
		}
	});
	
	if(execute){
		tamanhoWidthG = 0;
		$("#viz").empty();
		$("#viz").removeClass("efeito-adicional");
		var metodo = algoritmo;
		var action = $("#meuForm").attr("action");
		var form = $("#meuForm").serialize();
		var dadosTabela = table.$('input, text').serialize();
		console.log(form);
		$.ajax({
			type : "POST",
			url : action,
			dataType : "json",
			data : dadosTabela + "&" + form,
			success : function(json) {
				testeData = json;
				if(metodo == 'B'){
					criaTree(json.node, metodo);
					pesoInserido = constroiATabelaResposta(json.node.itens, metodo);
					$("#viz").addClass("efeito-adicional");
				}else if(metodo == 'G'){
					pesoInserido = constroiATabelaResposta(json.list, metodo);
				}else if(metodo == 'D'){
					pesoInserido = constroiATabelaResposta(json.respostaDinamica.itens, metodo);
					constroiMatrizRespostaDinamica(json.respostaDinamica.matriz);
					$("#viz").addClass("efeito-adicional");
				}else if(metodo == 'BB'){
					criaTree(json.node, metodo);
					pesoInserido = constroiATabelaResposta(json.node.itens, metodo);
					$("#viz").addClass("efeito-adicional");
				}else if(metodo == 'T'){
					constroiATabelaResposta(json.list, metodo);
				}
				
				if(pesoInserido){
					modificaMochila((pesoInserido * 100) / pesoTotal)
				}
			},
			error : function(jqXHR, textStatus) {
				alert("jqXHR:" + jqXHR);
				alert("erro:" + textStatus);
			},
			statusCode : {
				404 : function() {
					alert("page not found");
				}
			}
		});
	}else {
		alert("Todos os valores devem ser maior que 0.");
	}
}


function criaDataTables(){
	$("#showKnapsack").hide();
	$("#divResposta").hide();
	$("#viz").hide();
	var d = $("#qtdItens").val();
	var table = $("#itens").DataTable({
		"autoWidth" : true,
		"ordering" : false,
		"searching" : false,
		"columns" : [ {
			"width" : "2%",
			"className" : "dt-body-center"
		}, {
			"width" : "4%",
			"className" : "dt-body-center"
		}, {
			"width" : "4%",
			"className" : "dt-body-center"
		}, {
			"width" : "4%",
			"className" : "dt-body-center"
		} ]
	});
	modificaTamanhoTable(d, table);
	return table;
}
$(function() {
	
	$("input[type='text']").blur(function(){
		if(!$.isNumeric(this.value)){
			this.value = "";
		}
	});
	
	table = criaDataTables();	
	$("#qtdItens").change(function() {
		table.clear();
		table.destroy();
		table = criaDataTables();
	});
	$('#itens').on( 'page.dt', function () {
		$('#itens').on('draw.dt', function(){
			if(tentando){
				$("#itens input[name='checkTentar']").removeAttr('disabled');
			}else {
				$("#itens input[name='checkTentar']").prop('checked',false);
				$("#itens input[name='checkTentar']").prop('disabled',true);
				$("#itens input[type='text']").removeAttr('disabled');
			}			
		});
	});
	
	$("#itens tbody").on( 'click',"input[name='checkTentar']", function () {
		var row = $(this).parent().parent();
	    var dados = table.row( row ).data();
	    var itemId = $(this).val();
	    
	    var inputValor = $("#"+$(dados[1]).attr('id'));
	    var inputPeso = $("#"+$(dados[2]).attr('id'));
	    
	    var valor = parseInt($(inputValor).val());
	    var peso = parseInt($(inputPeso).val());
	    
	    if(valor <= 0 || peso <=0){
	    	$(this).prop('checked',false);
	    	return;
	    }
	    
	    var sit = $(this).prop('checked');
	    	    
	    if(sit){
	    	if(pesoInserido + peso <= pesoTotal){
	    		valorInserido += valor;
	    		pesoInserido += peso;	    		
	    	}else {
	    		$(this).prop('checked',!sit);
	    		alert('este item não cabe na mochila');
	    		return;
	    	}
	    }else {
	    	valorInserido -= valor;
	    	pesoInserido -= peso;
	    }
	    
	    setaItemEscolhidoNaTentativa(itemId, 1 * sit);
	    
	    $(inputValor).prop('disabled',sit);
		$(inputPeso).prop('disabled',sit);
	    
	    var porcentagem = (pesoInserido * 100)/pesoTotal;
	    modificaMochila(porcentagem);
	    
	});
	
	table.columns.adjust().draw();
	jQuery('ul.sf-menu').superfish({
		pathClass:	'current'
	});
	$('html').keypress(function(e){
		console.log(e.keyCode);
	});
	
});