var nodeAtual;
var nodeRaiz;
var selecionados = new Array();
var nodes = new Array();
var limite;
var solOtima;

function otima(itens, valor, peso){
	this.itensSol = itens;
	this.valor = valor ? valor : 0;
	this.peso = peso ? peso : 0;
}


function bound(itens, valor, peso, index, M){
	var b = valor;
	var c = peso;
	
	for (var i = index+1; i < itens.length; i++) {
		c += itens[i].peso;
		if(c <= M){
			b += itens[i].valor;
		}else {
			return (b + (1 - ((c-M)/itens[i].peso)) * itens[i].valor);
		}
	}
	return b;
}

function trataSentinel(itens){
	var node = nodeRaiz.children[0];
	nodeRaiz.children = node.children;
}

function setaSelecionados(itens){
	for (var i =1;i<selecionados.length;i++){
		itens[i-1].result = 1 * selecionados[i];
		itens[i-1].resultB = 1 * selecionados[i];
	}
}

function inicializaSelecionados(tamanho){
	selecionados = new Array(tamanho);
	for (var i=0;i<tamanho;i++){
		selecionados[i] = 0;
	}
}

function resetaItens(itens){
	var temSentinela = false;
	for ( var index in itens) {
		if(itens[index].id == 0)
			temSentinela = true;
		itens[index].resultB = 0;
	}
	
	return temSentinela;
}

function criaNodes(itens){
	itens.forEach(function(element,index,array){
		nodes.push(new node("Item "+element.id,0,0,new Array(),0));
	});
}

function executaBacktracking(itens, M){
	limite = 0;
	solOtima = new otima();
	nodeRaiz = new node("raiz",valorInserido,pesoInserido,new Array(),undefined);
	var verificandoBound = false;
	var itensCopia = itens.slice();
	itensCopia.splice(0,0,new item(0,0,0));
	var acabou = false;
	var pesoFinal = 0
		, valorFinal = -1
	var n = itensCopia.length
		, k = 0;
	inicializaSelecionados(n);
	
	nodeAtual = nodeRaiz;
	criaNodes(itensCopia);
	$('html').keypress(function(e){
		if(e.keyCode == 46 && !acabou){
			if(!verificandoBound){
				if((k < n) && ((pesoInserido + itensCopia[k].peso) <= M)){
					pesoInserido += itensCopia[k].peso;
					valorInserido += itensCopia[k].valor;
					itensCopia[k].resultB = 1;
					selecionados[k] = 1;
					
					var node_temp = new node("Item "+itensCopia[k].id,valorInserido, pesoInserido, new Array(), 1);
					if(nodeAtual.children == undefined){
						nodeAtual.children = new Array();
					}
					nodeAtual.children.splice(0,0, node_temp);
					nodes[k] = node_temp;
					nodeAtual = node_temp;
				}else if(k > n-1){
					pesoFinal = pesoInserido;
					valorFinal = valorInserido;
					k = n-1;
					setaSelecionados(itens);
				}else {
					selecionados[k] = 0;
					itensCopia[k].resultB = 0;
					var node_temp = new node("Item "+itensCopia[k].id,valorInserido, pesoInserido, new Array(), 0);
					if(nodeAtual.children == undefined){
						nodeAtual.children = new Array();
					}
					nodeAtual.children.splice(0,0, node_temp);
					nodes[k] = node_temp;
					nodeAtual = node_temp;
				}
			}
			limite = bound(itensCopia,valorInserido,pesoInserido,k,M);
			if(limite <= valorFinal){
				verificandoBound = true;
				while(k !=0 && selecionados[k] != 1){
					k--;
				}
				if(k == 0){
					trataSentinel(itensCopia);
					acabou = true;
					
					setaSelecionados(itens);
					trataSolucaoOtimaBacktracking(atualizatabelaDetalhada(itens,atualizaArvore(nodeRaiz)));
				}else {
					nodeAtual = nodes[k-1];
					itensCopia[k].resultB = 1;
					selecionados[k] = 0;
					pesoInserido -= itensCopia[k].peso;
					valorInserido -= itensCopia[k].valor;
					
					var node_temp = new node("Item "+itensCopia[k].id,valorInserido, pesoInserido, new Array(), 0);
					nodeAtual.children.splice(0,0, node_temp);
					if(nodeAtual.children == undefined){
						nodeAtual.children = new Array();
					}
					nodes[k] = node_temp;
					nodeAtual = node_temp;
					
					setaSelecionados(itens);
					trataSolucaoOtimaBacktracking(atualizatabelaDetalhada(itens,atualizaArvore(nodeRaiz)));				
				}
			}else {
				verificandoBound = false;
				if(k == 0 && !acabou){
					iniciaArvore(nodeRaiz);
				}else {
					setaSelecionados(itens);
					trataSolucaoOtimaBacktracking(atualizatabelaDetalhada(itens,atualizaArvore(nodeRaiz)));				
				}
				k++;
			}
			
		}else if(e.keyCode == 44){
			$("#viz").empty();
			$("#viz").removeClass("efeito-adicional");
			pesoFinal = 0
			valorFinal = -1
			pesoInserido = 0
			valorInserido = 0;
			k = 0;
			itensCopia = itens.slice();
			itensCopia.splice(0,0,new item(0,0,0));
			n = itensCopia.length;
			inicializaSelecionados(n);
			nodeRaiz = new node("raiz",valorInserido,pesoInserido,new Array(),undefined);
			nodeAtual = nodeRaiz;
			nodes = new Array();
			criaNodes(itens);
			acabou = false;
			solOtima = new otima();
			limite = 0;
		
		}
		if(acabou){
			setaSolucaoOtimaBacktracking();
		}
		
		
	});	
}


