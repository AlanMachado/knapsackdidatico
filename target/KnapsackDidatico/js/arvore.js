var margin;
var i = 0, duration = 750, root;
var tree;
var vis;
var diagonal;
var metodoEscolhido;
var tamanhoWidthG = 0;

function collapse(d) {
	if (d.children) {
		d._children = d.children;
		d._children.forEach(collapse);
		d.children = null;
	}
};

function click(d) {
	if (d.children) {
		d._children = d.children;
		d.children = null;
	} else {
		d.children = d._children;
		d._children = null;
	}
	update(d);
};

function defineNome(d){
	
	var texto = d.name;
	if(metodoEscolhido == 'B' && d.result != undefined){
		texto = new String(d.name+" cw: "+d.cw+" cv: "+d.cp); 
	}else if(metodoEscolhido == 'BB' && d.result != undefined){
		texto = new String(d.name+" lb: "+d.lb+" ub: "+d.ub);
	}
	
	return texto;
};

function update(source) {
	//verificar esse .reverse()
	var nodes = tree.nodes(root).reverse(), links = tree.links(nodes);

	nodes.forEach(function(d) {
		d.y = d.depth * 120;
	});

	var node = vis.selectAll("g.node").data(nodes, function(d) {
		return d.id || (d.id = ++i);
	});

	var nodeEnter = node.enter().append("g").attr("class", "node").attr("transform", function(d) {
				return "translate(" + source.y0 + "," + source.x0 + ")";
	}).on("click", click);
	
	nodeEnter.append("circle").attr("r", 1e-6).style("fill", function(d) {
		return d._children ? "lightsteelblue" : "#fff";
	}).style("stroke", function(d){
		return d.result == 0 ? "#BF2715" : "steelblue";
	});
	
	nodeEnter.append("text").attr("x", function(d) {
		return d.children || d._children ? -8 : 8;
	}).attr("dy", ".35em").attr("text-anchor", function(d) {
		return d.children || d._children ? "end" : "start";
	}).text(function(d) {
			//return d.result != undefined ? new String(d.name+" cw: "+d.cw+" cv: "+d.cp) : d.name;
			if(d.y > tamanhoWidthG){
				tamanhoWidthG = d.y; 
			}
			return defineNome(d);
	}).style("fill-opacity", 1e-6);

	var nodeUpdate = node.transition().duration(duration).attr("transform",
			function(d) {
				return "translate(" + d.y + "," + d.x + ")";
			});

	nodeUpdate.select("circle").attr("r", 4.5).style("fill", function(d) {
		return d._children ? "lightsteelblue" : "#fff";
	});

	nodeUpdate.select("text").style("fill-opacity", 1);

	var nodeExit = node.exit().transition().duration(duration).attr(
			"transform", function(d) {
				return "translate(" + source.y + "," + source.x + ")";
			}).remove();

	nodeExit.select("circle").attr("r", 1e-6);

	nodeExit.select("text").style("fill-opacity", 1e-6);

	var link = vis.selectAll("path.link").data(links, function(d) {
		return d.target.id;
	});

	link.enter().insert("path", "g").attr("class", "link").attr("d",
			function(d) {
				var o = {
					x : source.x0,
					y : source.y0
				};
				return diagonal({
					source : o,
					target : o
				});
			});

	link.transition().duration(duration).attr("d", diagonal);

	link.exit().transition().duration(duration).attr("d", function(d) {
		var o = {
			x : source.x,
			y : source.y
		};
		return diagonal({
			source : o,
			target : o
		});
	}).remove();

	nodes.forEach(function(d) {
		d.x0 = d.x;
		d.y0 = d.y;
	});
};

function criaTree(treeData, metodo) {
	metodoEscolhido = metodo;
	var widthParent = $("#viz").width();

	margin = {
		top : 20,
		right : 80,
		bottom : 20,
		left : 80
	}, width = widthParent - margin.right - margin.left, height = 800
			- margin.top - margin.bottom;

	tree = d3.layout.tree().size([ height, width ]);

	diagonal = d3.svg.diagonal().projection(function(d) {
		return [ d.y, d.x ];
	});

	vis = d3.select("#viz").append("svg:svg").attr("width",
			width + margin.right + margin.left).attr("height",
			height + margin.top + margin.bottom).append("svg:g").attr(
			"transform", "translate(" + margin.left + "," + margin.top + ")");

	root = treeData;
	root.x0 = height;
	root.y0 = 0;

	//root.children.forEach(collapse); para a arvore iniciar toda exposta essa linha teve de ser comentada.
	update(root);
	$("svg").width(parseInt(tamanhoWidthG) + 300);
	//$("#viz").width(parseInt(tamanhoWidthG) + 300);
}