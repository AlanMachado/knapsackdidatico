function preparaTelaParaExecucaoDeMetodo(){
	$("#elementoMochila").hide();
	$("#divResposta").show();
	$("#showKnapsack").show();
	$("#viz").show();
};

function preparaTelaParaTodos(){
	$("#elementoMochila").hide();
	$("#divResposta").show();
	$("#showKnapsack").hide();
	$("#viz").hide();
};

function preparaTelaParaPercorrer(vizTrue){
	$("#elementoMochila").hide();
	$("#divResposta").show();
	$("#showKnapsack").show();
	$("#viz").show();		
	$("#viz").empty();
	if(vizTrue){
		$("#viz").removeClass("efeito-adicional");
	}else {
		$("#viz").hide();
	}
};

function preparaVizualizacaoDoProblema(){
	$("#elementoMochila").show();
	$("#divResposta").hide();
	$("#showKnapsack").hide();
	$("#viz").hide();
};

function preencheItens(valores, pesos){
	var vector = table.$('.valores');
	var countInput = 1;
	var countValor = 0;
	var countPeso = 0;
	vector.each(function(d,e){
		if(countInput % 2 == 0){
			$(e).val(pesos[countPeso]);
			countPeso++;
		}else {
			$(e).val(valores[countValor]);
			countValor++;
		}
		countInput++;
	});
};


$(function() {
	
	$(".naoTentando").click(function(){
		saindoTentativa();
	});
	
	$(".naoPercorrendo").click(function(){
		$('html').unbind('keypress');
	})
	
	$("#definirMochila").click(function(){
		preparaVizualizacaoDoProblema();
	});
	
	$("#tentativa").click(function(){
		$("#elementoMochila").show();
		$("#divResposta").show();
		$("#showKnapsack").show();
		$("#viz").hide();
		habilitaTentativa();
	});
	
	$("#executarGuloso").click(function(){
		preparaTelaParaExecucaoDeMetodo();
		$("#defineMetodo").val("G");
		executaMetodo("G");
	});
	
	$("#executarBacktracking").click(function(){
		preparaTelaParaExecucaoDeMetodo();
		$("#defineMetodo").val("B");
		executaMetodo("B");
	});
	
	$("#executarBranch").click(function(){
		preparaTelaParaExecucaoDeMetodo();
		$("#defineMetodo").val("BB");
		executaMetodo("BB");
	});
	
	$("#executarDinamico").click(function(){
		preparaTelaParaExecucaoDeMetodo();
		$("#defineMetodo").val("D");
		executaMetodo("D");
	});
	
	$("#executarTodos").click(function(){
		preparaTelaParaTodos();
		$("#defineMetodo").val("T");
		executaMetodo("T");
	});
	
	$("#exemplo1").click(function(){
		preparaVizualizacaoDoProblema();
		var importancias = new Array(500,400,300,450);
		var pesos = new Array(4,2,1,3);
		$("#qtdItens").val(4);
		$("#pesoMochila").val(5);
		$("#qtdItens").trigger("change");
		preencheItens(importancias, pesos);
	});
	
	$("#exemplo2").click(function(){
		preparaVizualizacaoDoProblema();
		var importancias = new Array(11,21,31,33,43,53,55,65);
		var pesos = new Array(1,11,21,23,33,43,45,55);
		$("#qtdItens").val(8);
		$("#pesoMochila").val(110);
		$("#qtdItens").trigger("change");
		preencheItens(importancias, pesos);
	});
	
	$("#percorreGuloso").click(function(){
		$('html').unbind('keypress');
		preparaTelaParaPercorrer();
		passByGuloso();
		trataPercorrer("G");
	});
	
	$("#percorreBacktracking").click(function(){
		$('html').unbind('keypress');
		preparaTelaParaPercorrer(true);
		passByBacktracking();
		trataPercorrer("B");
	});
	
	$("#percorreBranchBound").click(function(){
		$('html').unbind('keypress');
		preparaTelaParaPercorrer(true);
		passByBranchBound();
		trataPercorrer("BB");
	});
	
	$("#percorreDinamico").click(function(){
		$('html').unbind('keypress');
		preparaTelaParaPercorrer(true);
		passByDinamico();
		trataPercorrer("D");
	})
	
});