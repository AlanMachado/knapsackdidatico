function node(name, cp, cw, children, result) {
	this.name = name;
	this.cp = cp;
	this.cw = cw;
	this.children = children;
	this.result = result;
}

function node_bb(name,children,parent,level,capacidadeDisponivel,pe,ub,lb,tag,item){
	this.name = name;
	this.children = children;
	this.parent = parent;
	this.level = level;
	this.capacidadeDisponivel = capacidadeDisponivel;
	this.pe = pe;
	this.ub = ub;
	this.lb = lb;
	this.tag = tag;
	this.item = item;
	this.result = tag;
}


function compara_no(a, b){
	if(a.ub > b.ub){
		return 1;
	}else if(a.ub < b.ub){
		return -1;
	}
	return 0;
}