var matrizResultante;

function criaMatrizResultante(tamanhoLinha, tamanhoColuna){
	$("#viz").addClass("efeito-adicional");
	$("#viz").append(meRetorneUmaTable("matrizDinamica"));
	var meuHead = $("#matrizDinamica thead tr");
	for (var sub = 0; sub < tamanhoColuna; sub++) {
		meuHead.append("<th>"+ sub +"</th>");		
	}
	
	matrizResultante = criarDataTable($("#matrizDinamica"), 1400);
	
	var linha;
	for (var i = 0; i < tamanhoLinha; i++){
		linha = new Array();
		for (var j = 0; j < tamanhoColuna; j++) {
			linha.push(0);
			if(i != 0 && j != 0){ // inicializa coluna e linha 0
				linha[j] = -1; // o resto da matriz será inicializada com -1
			}
		}
		matrizResultante.row.add(linha).draw().node();
	}
}

function zeraResult(itens){
	itens.forEach(function(element,index,a){
		itens[index].result = 0;
	});
}

function removeColor(){
	$("#matrizDinamica tbody tr td").removeClass("coloBacktrace");
}

function addColor(l, c){
	$("#matrizDinamica tbody tr:nth-child("+l+") td:nth-child("+c+")").addClass("coloBacktrace");
}

function backtrace(l, c, itens){
	var i=l, j=c;
	pesoInserido = 0;
	valorInserido = 0;
	var linha, linhaAcima;
	zeraResult(itens);
	removeColor();
	while((i > 0)  && (j > 0)){
		itens[i-1].resultD = 0;
		itens[i-1].result = 0
		linhaAcima = matrizResultante.row(i-1).data();
		linha = matrizResultante.row(i).data();
		if(linha[j] != linhaAcima[j]){
			itens[i-1].result = 1;
			pesoInserido += itens[i-1].peso;
			valorInserido += itens[i-1].valor;
			addColor(i,j+1);
			addColor(i+1,j+1);
			j -= itens[i-1].peso;				
		}
		i--;
	}
}

function atualizaMatrizResultante(linha, linhaAcima, l){
	matrizResultante.row(l-1).data(linhaAcima).draw().node();
	matrizResultante.row(l).data(linha).draw().node();
}

function executaDinamico(itens, M){
	var tamanhoLinha = itens.length + 1;
	var tamanhoColuna = M+1;
	criaMatrizResultante(tamanhoLinha, tamanhoColuna);
	var acabou = false;
	var l = 1;
	$('html').keypress(function(e){
		if(e.keyCode == 46 && !acabou){
			var linha;
			var linhaAcima;
			var item;
			if(l < tamanhoLinha){
				linhaAcima = matrizResultante.row(l-1).data();
				linha = matrizResultante.row(l).data();
				item = itens[l-1];
				for(var c = 1; c< tamanhoColuna; c++){
					linha[c] = linhaAcima[c];
					
					if(item.peso <= c){
						if((item.valor + linhaAcima[c-item.peso]) > linhaAcima[c]){
							linha[c] = item.valor + linhaAcima[c-item.peso];
						}
					}
				}
				atualizaMatrizResultante(linha, linhaAcima, l);
				backtrace(l,M, itens);
				l++;
			}else {
				acabou = true;
			}
			atualizatabelaDetalhada(itens,atualizaSeta());

		}else if (e.keyCode == 44){
			$("#viz").empty();
			$("#viz").removeClass("efeito-adicional");
			tamanhoLinha = itens.length + 1;
			tamanhoColuna = M+1;
			criaMatrizResultante(tamanhoLinha, tamanhoColuna);
			acabou = false;
			l = 1;
			c = 1;
		}
		
	});
	
}