package org.knapsack.algoritmos;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.knapsack.model.Item;
import org.knapsack.model.Node;
import org.knapsack.model.NodeComparator;

public class BranchAndBound {
	
	NodeComparator comp = new NodeComparator();
	Node raiz = null;
	List<Node> nodes = new ArrayList<Node>();
	List<Node> expandir = new ArrayList<Node>();
	static Float LBB = 0f, UBB = 0f, L;
	DecimalFormat formatador = new DecimalFormat("0.00");
	
	private void formataLimites(){
		UBB = Float.valueOf(formatador.format(UBB).replace(",", "."));
		LBB = Float.valueOf(formatador.format(LBB).replace(",", "."));
	}
	
	/**
	 * Algorithm to compute lower and upper bounds 
	 * @param item
	 * @param pesoRestante = RW "remaining capacity"
	 * @param valorCorrente = CP "current potency"
	 * @param k
	 * @param LBB = -u(X)
	 * @param UBB = -v(X)
	 * @return
	 */
	private void lubound(List<Item> itens,Integer pesoRestante, Double valorCorrente, Integer k){
		Float c = Float.valueOf(pesoRestante);
		LBB = valorCorrente.floatValue();
		Integer n = itens.size();
		for (int i = k; i < n; i++) {
			Item temp = itens.get(i);
			if(c < temp.getPeso()){
				UBB = LBB + (c * temp.getChave());
				for (int j = i+1; j < n; j++) {
					Item subTemp = itens.get(j);
					if(c >= subTemp.getPeso()){
						c -= subTemp.getPeso();
						LBB += subTemp.getImportancia();
					}
				}
				formataLimites();
				return;
			}
			c -= temp.getPeso();
			LBB += temp.getImportancia();
		}
		UBB = LBB;
		formataLimites();
	}
	
	private void criaNodes(List<Item> itens){
		nodes.add(new Node("raiz"));
		expandir.add(nodes.get(0));
		raiz = expandir.get(0);
	}
	
	private Node getLargest(){
		expandir.sort(comp);
		Node node = expandir.remove(expandir.size()-1);
		nodes.add(node);
		return node;
	}
	
	private void finish(Float L, Node ans, Integer qtdItens){
		Node temp = ans;
		do{
			if(temp.getItem() != null){
				temp.getItem().setResultBB(temp.getTag());
			}
			temp = temp.getParent();
		}while(temp != null);
	}
	
	private void newNode(Item item,Node parent, Integer level, Integer tag, Integer capacidadeDisponivel, Integer pe, Float ub, Float LBB){
		Node newNode = new Node("item " + item.getId());
		newNode.setParent(parent);
		newNode.setLevel(level);
		newNode.setTag(tag);
		newNode.setCapacidadeDisponivel(capacidadeDisponivel);
		newNode.setPe(pe);
		newNode.setUb(ub);
		newNode.setLb(LBB);
		newNode.setItem(item);
		parent.addChildrenBB(newNode);
		expandir.add(newNode);
	}
	
	private Node getNode(){
		return expandir.remove(0);
	}
	
	public Node execute(List<Item> itens, Integer M, Integer s){
		Collections.sort(itens);
		itens.add(0,new Item(0, 0, 0)); //sentinela
		Integer i, prof, cap;
		Node ans = null;
		Integer n = itens.size();
		criaNodes(itens);
		
		Node nodeAtual = getNode();
		nodeAtual.setLevel(1);
		nodeAtual.setCapacidadeDisponivel(M);
		nodeAtual.setPe(0);
		lubound(itens, M, 0.0, 1);
		L = LBB - s;
		nodeAtual.setUb(UBB);
		do{
			i = nodeAtual.getLevel();
			cap = nodeAtual.getCapacidadeDisponivel();
			prof = nodeAtual.getPe();
			if(i.equals(n)){
				if(prof > L){
					L = prof.floatValue();
					ans = nodeAtual;
				}
			}else { 
				if(cap >= itens.get(i).getPeso()){
					newNode(itens.get(i), 
							nodeAtual, 
							nodeAtual.getLevel() + 1, 
							1, 
							cap - itens.get(i).getPeso(), 
							prof + itens.get(i).getImportancia(), 
							nodeAtual.getUb(),
							LBB);
				}
				
				lubound(itens, cap, prof.doubleValue(), i+1);
				if(UBB > L){  
					newNode(itens.get(i), 
							nodeAtual, 
							nodeAtual.getLevel()+1, 
							0, 
							cap, 
							prof, 
							UBB,
							LBB);
					L = Math.max(L, LBB-s);
				}
			}
			if (!expandir.isEmpty()){
				nodeAtual = getLargest();
			}
		}while(nodeAtual.getUb() > L);
		finish(L, ans, n);
		itens.remove(0);
		raiz.setItens(itens);
		return raiz;
	}
	
}
