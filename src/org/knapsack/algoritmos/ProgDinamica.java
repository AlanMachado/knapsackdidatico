package org.knapsack.algoritmos;

import java.util.List;

import org.knapsack.model.Item;
/**
 * 
 * @author Alan
 *
 */
public class ProgDinamica {

	/**
	 * 
	 * @param itens
	 * @param M = capacidade da mochila
	 */
	public Integer[][] execute(List<Item> itens, Integer M){
		Integer tamanhoLinhaMatriz = itens.size()+1;
		Integer tamanhoColunaMatriz = M+1;
		Integer [][] matriz = new Integer [tamanhoLinhaMatriz][tamanhoColunaMatriz];
		
		for (int i = 0; i < tamanhoLinhaMatriz; i++){
			for (int j = 0; j < tamanhoColunaMatriz; j++) {
				matriz[i][j] = 0;
				if(i != 0 && j != 0){ // inicializa coluna e linha 0
					matriz[i][j] = -1; // o resto da matriz será inicializada com -1
				}
			}
		}
		
		for (int i = 1; i < tamanhoLinhaMatriz; i++) {
			for (int c = 1; c < tamanhoColunaMatriz; c++) {
				matriz[i][c] = matriz[i-1][c];
				if(itens.get(i-1).getPeso() <= c){
					if((itens.get(i-1).getImportancia() + matriz[i-1][c-itens.get(i-1).getPeso()]) > matriz[i-1][c]){
						matriz[i][c] = itens.get(i-1).getImportancia() + matriz[i-1][c-itens.get(i-1).getPeso()];
					}
				}	
			}
		}
		//Calcula os elementos que fazem parte da resposta
		Integer i = itens.size();
		Integer j = M;
		while((i > 0)  && (j > 0)){
			itens.get(i-1).setResultD(0);
			if(matriz[i][j] != matriz[i-1][j]){
				itens.get(i-1).setResultD(1);
				j -= itens.get(i-1).getPeso();
				
			}
			i--;
		}
		
		return matriz;
	}
}
