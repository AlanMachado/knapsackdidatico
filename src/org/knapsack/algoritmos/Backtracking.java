package org.knapsack.algoritmos;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.knapsack.model.Item;
import org.knapsack.model.Node;

/**
 * descobrir o que deve ser p, w, k, fp e fw
 * @author Alan
 *
 */
public class Backtracking {
	
	Node raiz = new Node("raiz");
	List<Node> nodes = new ArrayList<Node>();
	
	/**
	 * de forma gulosa verifica quais itens ainda cabem na mochila
	 * faz uma porcentagem do peso que sobra e multiplica pelo valor do primeiro item que ja nao cabe (porcentagem do valor do item que caberia no restante da mochila) 
	 * soma com o valor acumulado. isto eh o limite superior
	 * @param itens
	 * @param p = importancia atual total
	 * @param w = peso atual total
	 * @param k = indice atual
	 * @param M = capacidade da mochila
	 * @return
	 */
	private float bound(List<Item> itens,float p, float w,Integer k, float M){
		float b = p;
		float c = w;
		for (int i = k + 1; i < itens.size(); i++) {
			c += itens.get(i).getPeso();
			if(c <= M){
				b += itens.get(i).getImportancia();
			}else {
				return (b + (1 - ((c - M)/itens.get(i).getPeso())) * itens.get(i).getImportancia());
			}
		}
		return b;
	}
	
	private void setaSelecionados(List<Item> itens, Integer [] selecionados){
		for (int i = 0; i < selecionados.length; i++) {
			itens.get(i).setResultB(selecionados[i]);
		}
	}
	
	private void criaNodes(List<Item> itens){
		//nodes.add(raiz);
		for (Item item : itens) {
			nodes.add(new Node("Item "+item.getId()));
		}
	}
	
	private void trataSentinela(Node raiz,List<Item> itens){
		itens.remove(0);
		Node temp = raiz.getChildren().get(0);
		raiz.setChildren(temp.getChildren());
	}
	
	/**
	 * 
	 * @param itens
	 * @param fw = peso final(começa com zero), no livro, final weigth 
	 * @param fp = importancia final (começa com zero), no livro, final potency
	 * @param M = capacidade da mochila
	 *
	 */
	public Node execute(List<Item> itens, float fw, float fp, float M){
		Collections.sort(itens);
		itens.add(0, new Item(0, 0, 0)); // adicionando sentinela,testar mais
		criaNodes(itens);
		float cw = 0.0f;
		float cp = 0.0f;
		Integer k = 0;
		Integer n = itens.size();
		Integer [] selecionados = new Integer[n];
		fp = -1;
		
		Node nodeAtual = raiz;
		while(true){
			while((k < n) && ( (cw + itens.get(k).getPeso()) <= M)){ //faz movimentos para filhos a esquerda
				cw += itens.get(k).getPeso();
				cp += itens.get(k).getImportancia();
				selecionados[k] = 1; // coloca elemento na mochila
				
				Node temp = new Node("Item "+itens.get(k).getId());
				nodeAtual = nodeAtual.addChildrenBacktracking(temp ,1, cp, cw);
				nodes.set(k, temp);
				k++;
				
			}
			if(k > n-1){
				fp = cp;
				fw = cw;
				k = n-1;
				setaSelecionados(itens, selecionados);
			}else {
				selecionados[k] = 0;
				
				Node temp = new Node("Item "+itens.get(k).getId());
				nodeAtual = nodeAtual.addChildrenBacktracking(temp,0, cp, cw);
				nodes.set(k, temp);
			}
			while (bound(itens, cp, cw, k, M) <= fp) {
				while (k != 0 && selecionados[k] != 1) {
					k--;
				}
				if(k == 0){
					trataSentinela(raiz, itens);
					raiz.setItens(itens);
					return raiz;
				}
				nodeAtual = nodes.get(k-1);
				
				selecionados[k] = 0;
				cw -= itens.get(k).getPeso();
				cp -= itens.get(k).getImportancia();
				
				Node temp = new Node("Item "+itens.get(k).getId());
				nodeAtual = nodeAtual.addChildrenBacktracking(temp, 0, cp, cw);
				nodes.set(k, temp);
				
			}
			k++;
		}
	}
}


