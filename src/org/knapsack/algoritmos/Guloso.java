package org.knapsack.algoritmos;

import java.util.Collections;
import java.util.List;

import org.knapsack.model.Item;

public class Guloso {

	/**
	 * 
	 * @param itens
	 * @param M = capacidade da mochila
	 */
	public void execute(List<Item> itens,Integer M){
		Collections.sort(itens);
		Integer CM = M;
		Integer i = 0,j = 0;
		for (i = 0;i < itens.size();i++) {
			if(itens.get(i).getPeso() > CM){
				break;
			}
			itens.get(i).setResultG(1);
			CM -= itens.get(i).getPeso();
		}
		j = i+1;
		while (j < itens.size()) {
			if(itens.get(j).getPeso() <= CM){
				CM -= itens.get(j).getPeso();
				itens.get(j).setResultG(1);
			}
			j++;
		}
	}
}
