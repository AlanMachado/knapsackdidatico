package org.knapsack.algoritmos;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.knapsack.model.Item;

public class Teste {
	
	static Random gerador = new Random();
	static List<Item> itens;  
	
	public static void testeBranchBound(List<Item> itens, Integer M){
		BranchAndBound b = new BranchAndBound();
		b.execute(itens, M, 1);
	}
	
	
	public static void testeBacktracking(List<Item> itens, Integer M){
		float Tpeso = 0; //fw
		float Timportancia = 0; //fp
		
		Backtracking bknap = new Backtracking();
		bknap.execute(itens, Tpeso, Timportancia, M);
	}
	
	public static void testeDinamico(List<Item> itens, Integer M){
		ProgDinamica pr = new ProgDinamica();
		pr.execute(itens, M);
	}
	
	public static void testeGuloso(List<Item> itens, Integer M){
		Guloso greedy = new Guloso();
		greedy.execute(itens, M);
	}
	
	public static Integer constroiArrayDeItems(int qtd){
		itens = new ArrayList<Item>();
		Integer pesoTotal = 0; 
		for (int i = 0; i < qtd; i++) {
			int valor = gerador.nextInt(100) + 1;
			int peso = gerador.nextInt(100) + 1;
			pesoTotal += peso;
			itens.add(new Item(i, valor, peso));
		}
		
		return pesoTotal;
	}
	
	public static void exibeResultado(String metodo){
		for (Item item : itens) {
			if(metodo.equals("G"))
				System.out.println(item + " " + item.getResultG());
			if(metodo.equals("B"))
				System.out.println(item + " " + item.getResultB());
			if(metodo.equals("D"))
				System.out.println(item + " " + item.getResultD());
			if(metodo.equals("BB"))
				System.out.println(item + " " + item.getResultBB());
		}
	}
	
	public static void main(String[] args) {
		/*List<Long> temposGuloso = new ArrayList<Long>();
		List<Long> temposBranchAndBound = new ArrayList<Long>();
		List<Long> temposBackTracking = new ArrayList<Long>();
		List<Long> temposDinamico = new ArrayList<Long>();*/
		Long inicio;
		Long fim;
		Integer pesoMochila;
		/*System.out.println("---Guloso---");
		for (int i = 0; i < 1000; i++) {
			pesoMochila = constroiArrayDeItems(1000)/2;
			inicio = System.currentTimeMillis();
			testeGuloso(itens, pesoMochila);
			fim = System.currentTimeMillis();
			temposGuloso.add(fim-inicio);			
		}
		
		for (Long tempo : temposGuloso) {
			System.out.println(tempo);
		}*/
		
		/*System.out.println("---BackTracking---");
		
		for (int i = 0; i < 1000; i++) {
			pesoMochila = constroiArrayDeItems(1000)/2;
			inicio = System.currentTimeMillis();
			testeBacktracking(itens, pesoMochila);
			fim = System.currentTimeMillis();
			temposBackTracking.add(fim-inicio);
		}
		
		for (Long tempo : temposBackTracking) {
			System.out.println(tempo);
		}*/
		
		/*System.out.println("---Dinamico---");
		for (int i = 0; i < 1000; i++) {
			pesoMochila = constroiArrayDeItems(1000)/2;
			inicio = System.currentTimeMillis();
			testeDinamico(itens, pesoMochila);
			fim = System.currentTimeMillis();
			temposDinamico.add(fim-inicio);
		}
		
		for (Long tempo : temposDinamico) {
			System.out.println(tempo);
		}*/
		
		
		/*System.out.println("---Branch-And-Bound---");
		for (int i = 0; i < 1000; i++) {
			pesoMochila = constroiArrayDeItems(1000)/2;
			inicio = System.currentTimeMillis();
			testeBranchBound(itens, pesoMochila);
			fim = System.currentTimeMillis();
			temposBranchAndBound.add(fim-inicio);
		}
		
		for (Long tempo : temposBranchAndBound) {
			System.out.println(tempo);
		}*/
		/*pesoMochila = constroiArrayDeItems(100000)/2;
		
		inicio = System.currentTimeMillis();
		testeBranchBound(itens, pesoMochila);
		fim = System.currentTimeMillis();
		System.out.println(fim - inicio);
		
		inicio = System.currentTimeMillis();
		testeBacktracking(itens, pesoMochila);
		fim = System.currentTimeMillis();
		System.out.println(fim - inicio);*/
		
		/*pesoMochila = constroiArrayDeItems(3000)/2; // 5000 e deu java heap space
		inicio = System.currentTimeMillis();
		testeDinamico(itens, pesoMochila);
		fim = System.currentTimeMillis();
		System.out.println(fim-inicio);*/
		
	}
	
	
	
}
