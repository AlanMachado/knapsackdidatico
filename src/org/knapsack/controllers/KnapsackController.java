package org.knapsack.controllers;

import static br.com.caelum.vraptor.view.Results.json;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import org.knapsack.algoritmos.Backtracking;
import org.knapsack.algoritmos.BranchAndBound;
import org.knapsack.algoritmos.Guloso;
import org.knapsack.algoritmos.ProgDinamica;
import org.knapsack.model.Item;
import org.knapsack.model.ItemComparator;
import org.knapsack.model.Node;
import org.knapsack.model.RespostaDinamica;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Result;

@Controller
public class KnapsackController {

	@Inject
	private Result result;
	@Inject
	private RespostaDinamica resposta;
	@Inject
	private Backtracking bknap;
	@Inject
	private Guloso guloso;
	@Inject
	private ProgDinamica dinamica;
	@Inject
	private BranchAndBound branchBound;
	
	
	public void index(){}
	
	public void executar(List<Item> itens, String metodo, Integer peso){
		
		if("B".equalsIgnoreCase(metodo)){
			
			Node node = bknap.execute(itens, 0, 0, peso);
			result.use(json()).from(node).include("itens").include("children").serialize();
		
		}else if("G".equalsIgnoreCase(metodo)){
			
			guloso.execute(itens, peso);
			Collections.sort(itens, new ItemComparator());
			result.use(json()).from(itens).serialize();
		
		}else if("D".equalsIgnoreCase(metodo)){
			
			Integer [][] matriz = dinamica.execute(itens, peso);
			resposta.setItens(itens);
			resposta.setMatriz(matriz);
			result.use(json()).from(resposta).include("matriz").include("itens").serialize();
		
		}else if("BB".equalsIgnoreCase(metodo)){
			
			Node node = branchBound.execute(itens, peso, 1);
			result.use(json()).from(node).include("itens").include("children").serialize();
		}else if("T".equalsIgnoreCase(metodo)){
			
			bknap.execute(itens, 0, 0, peso);
			guloso.execute(itens, peso);
			dinamica.execute(itens, peso);
			branchBound.execute(itens, peso, 1);
			result.use(json()).from(itens).serialize();
		}
		
	}
}
