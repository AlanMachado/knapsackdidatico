package org.knapsack.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Node {
	
	// usado pelo backtracking
	private List<Item> itens;
	private String name;
	private List<Node> children;
	private Integer result;
	private float cp;
	private float cw;
	
	//usado pelo Branch and Bound
	private Node parent;
	private Integer level;
	private Integer capacidadeDisponivel;
	private Integer pe = 0;
	private Float ub;
	private Float lb;
	private Integer tag;
	private Item item;
	

	public Node() {}
	
	public Node(String name){
		this.name = name;
		children = new ArrayList<Node>();
	}
	
	public Node(String name, Integer result, float cp, float cw){
		this.name = name;
		this.result = result;
		children = new ArrayList<Node>();
		this.cp = cp;
		this.cw = cw;
	}
	
	public Node addChildrenBacktracking(Node n, Integer result, float cp, float cw){
		n.setResult(result);
		n.setCp(cp);
		n.setCw(cw);
		children.add(0,n);
		return n;
	}
	
	public void addChildrenBB(Node n){
		n.setResult(n.getTag());
		children.add(0,n);
	}
		
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Node> getChildren() {
		return children;
	}
	public void setChildren(List<Node> children) {
		this.children = children;
	}

	public Integer getResult() {
		return result;
	}

	public void setResult(Integer result) {
		this.result = result;
	}

	public float getCp() {
		return cp;
	}

	public void setCp(float cp) {
		this.cp = cp;
	}

	public float getCw() {
		return cw;
	}

	public void setCw(float cw) {
		this.cw = cw;
	}

	public List<Item> getItens() {
		return itens;
	}

	public void setItens(List<Item> itens) {
		Collections.sort(itens, new ItemComparator());
		this.itens = itens;
	}

	public Node getParent() {
		return parent;
	}

	public void setParent(Node parent) {
		this.parent = parent;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Integer getCapacidadeDisponivel() {
		return capacidadeDisponivel;
	}

	public void setCapacidadeDisponivel(Integer capacidadeDisponivel) {
		this.capacidadeDisponivel = capacidadeDisponivel;
	}

	public Integer getPe() {
		return pe;
	}

	public void setPe(Integer pe) {
		this.pe = pe;
	}

	public Float getUb() {
		return ub;
	}

	public void setUb(Float ub) {
		this.ub = ub;
	}

	public Integer getTag() {
		return tag;
	}

	public void setTag(Integer tag) {
		this.tag = tag;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public Float getLb() {
		return lb;
	}

	public void setLb(Float lb) {
		this.lb = lb;
	}
}
