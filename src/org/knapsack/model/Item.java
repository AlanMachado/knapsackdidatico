package org.knapsack.model;

public class Item implements Comparable<Item>{
	private Integer id;
	private Integer importancia;
	private Integer peso;
	private float chave;
	private Integer resultG;
	private Integer resultB;
	private Integer resultD;
	private Integer resultBB;
	
	public Item() {
		resultG = 0;
		resultB = 0;
		resultD = 0;
	}
	
	public Item(Integer importancia, Integer peso){
		this();
		this.importancia = importancia;
		this.peso = peso;
		this.chave = Float.valueOf(importancia)/Float.valueOf(peso);
	}
	
	public Item(Integer id,Integer importancia, Integer peso){
		this(importancia, peso);
		this.id = id;
	}
		
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getImportancia() {
		return importancia;
	}

	public void setImportancia(Integer importancia) {
		this.importancia = importancia;
	}

	public Integer getPeso() {
		return peso;
	}

	public void setPeso(Integer peso) {
		this.peso = peso;
	}

	public float getChave() {
		return chave;
	}

	public void setChave(float chave) {
		this.chave = chave;
	}

	public Integer getResultG() {
		return resultG;
	}

	public void setResultG(Integer resultG) {
		this.resultG = resultG;
	}

	public Integer getResultB() {
		return resultB;
	}

	public void setResultB(Integer resultB) {
		this.resultB = resultB;
	}

	public Integer getResultD() {
		return resultD;
	}

	public void setResultD(Integer resultD) {
		this.resultD = resultD;
	}

	public Integer getResultBB() {
		return resultBB;
	}

	public void setResultBB(Integer resultBB) {
		this.resultBB = resultBB;
	}

	// ordem decrescente
	@Override
	public int compareTo(Item o) {
		if(this.chave < o.chave){
			return 1;
		}else if(this.chave > o.chave){
			return -1;
		}
		return 0;
	}
	
	@Override
	public String toString() {
		return chave + " " + importancia + " " + peso;
	}
}
