package org.knapsack.model;

import java.util.List;

public class RespostaDinamica {
	private Integer [][] matriz;
	private List<Item> itens;
	
	public RespostaDinamica() {}
	
	public RespostaDinamica(Integer[][] matriz, List<Item> itens) {
		this.matriz = matriz;
		this.itens = itens;
	}

	public Integer[][] getMatriz() {
		return matriz;
	}
	public void setMatriz(Integer[][] matriz) {
		this.matriz = matriz;
	}
	public List<Item> getItens() {
		return itens;
	}
	public void setItens(List<Item> itens) {
		this.itens = itens;
	}
	
	
}
